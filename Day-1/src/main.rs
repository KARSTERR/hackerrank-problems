use std::io;

fn main() {
    // Declare second integer, double, and String variables.
    let mut i: i32 = 4;
    let mut d: f64 = 4.0;
    let s: String = String::from("HackerRank ");

    // Read and save an integer, double, and String to your variables.

    let mut i2 = read_input::<i32>();
    let mut d2 = read_input::<f64>();
    let mut s2 = read_input::<String>();

    // Print the sum of both integer variables on a new line.
    println!("{}", i + i2);

    // Print the sum of the double variables on a new line.
    println!("{}", d + d2);

    // Concatenate and print the String variables on a new line
    // The 's' variable above should be printed first.
    println!("{}{}", s, s2);
}

fn read_input<T>() -> T
    where
        T: std::str::FromStr,
        T::Err: std::fmt::Debug,
{
    let mut input = String::new();
    io::stdin().read_line(&mut input)
        .expect("Failed to read line");
    input.trim().parse()
        .expect("Please enter a valid value")
}
